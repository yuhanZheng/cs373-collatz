# CS 373: Software Engineering Collatz Repo

* Name: Yuhan Zheng

* EID: yz24347

* GitLab ID: yuhanZheng

* HackerRank ID: yuhanzheng

* Git SHA: 3c22489c28c2a54a190ae9a520bee81f790583cb

* GitLab Pipelines: https://gitlab.com/yuhanZheng/cs373-collatz/-/pipelines

* Estimated completion time: 7 hrs

* Actual completion time: 6 hrs

* Comments: the structure of get_cycle_length() function was perviouly written for the collatz project 
            in generic programming class.
