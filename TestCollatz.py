#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = too-many-public-methods

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read_0(self):
        self.assertEqual(collatz_read("1 10\n"), (1, 10))

    def test_read_1(self):
        self.assertEqual(collatz_read("999999 1\n"), (999999, 1))

    # ----
    # eval
    # ----

    def test_eval_1(self):
        self.assertEqual(collatz_eval((1, 10)), (1, 10, 20))

    def test_eval_2(self):
        self.assertEqual(collatz_eval((100, 200)), (100, 200, 125))

    def test_eval_3(self):
        self.assertEqual(collatz_eval((201, 210)), (201, 210, 89))

    def test_eval_4(self):
        self.assertEqual(collatz_eval((900, 1000)), (900, 1000, 174))

    def test_eval_5(self):
        self.assertEqual(collatz_eval((1000, 900)), (1000, 900, 174))

    def test_eval_6(self):
        self.assertEqual(collatz_eval((50001, 50500)), (50001, 50500, 265))

    def test_eval_7(self):
        self.assertEqual(collatz_eval((10, 10)), (10, 10, 7))

    def test_eval_8(self):
        self.assertEqual(collatz_eval((6, 10)), (6, 10, 20))

    def test_eval_9(self):
        self.assertEqual(collatz_eval((50000, 50501)), (50000, 50501, 265))

    def test_eval_10(self):
        self.assertEqual(collatz_eval((1, 2)), (1, 2, 2))

    def test_eval_11(self):
        self.assertEqual(collatz_eval((501, 2000)), (501, 2000, 182))

    def test_eval_12(self):
        self.assertEqual(collatz_eval((1, 2000)), (1, 2000, 182))

    def test_eval_13(self):
        self.assertEqual(collatz_eval((302, 303)), (302, 303, 43))

    def test_eval_14(self):
        self.assertEqual(collatz_eval((501, 600)), (501, 600, 137))

    def test_eval_15(self):
        self.assertEqual(collatz_eval((5, 10)), (5, 10, 20))

    # -----
    # print
    # -----

    def test_print_0(self):
        sout = StringIO()
        collatz_print(sout, (1, 10, 20))
        self.assertEqual(sout.getvalue(), "1 10 20\n")

    def test_print_1(self):
        sout = StringIO()
        collatz_print(sout, (1, 1, 1))
        self.assertEqual(sout.getvalue(), "1 1 1\n")

    # -----
    # solve
    # -----

    def test_solve_0(self):
        sin = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        sout = StringIO()
        collatz_solve(sin, sout)
        self.assertEqual(
            sout.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )

    def test_solve_1(self):
        sin = StringIO("1 10\n1 1\n10 1\n6 10\n")
        sout = StringIO()
        collatz_solve(sin, sout)
        self.assertEqual(sout.getvalue(), "1 10 20\n1 1 1\n10 1 20\n6 10 20\n")


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
